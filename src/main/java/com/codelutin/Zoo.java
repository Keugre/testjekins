package com.codelutin;

import java.util.ArrayList;

public class Zoo {

    protected String nom;
    protected String adresse;
    protected ArrayList<Animal> animaux;

    public Zoo(){

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public ArrayList<Animal> getAnimaux() {
        return animaux;
    }

    public void setAnimaux(ArrayList<Animal> animaux) {
        this.animaux = animaux;
    }

    public void ajouterOiseau(Oiseau oiseau){
        animaux.add((Animal)oiseau);
    }
}
