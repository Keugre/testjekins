package com.codelutin;

public class MammifereOvipare extends Animal implements Ovipare{

    protected int moisAllaitement;
    protected String locomotion;

    public MammifereOvipare(){

    }

    public int getMoisAllaitement() {
        return moisAllaitement;
    }

    public void setMoisAllaitement(int moisAllaitement) {
        this.moisAllaitement = moisAllaitement;
    }

    public String getLocomotion() {
        return locomotion;
    }

    public void setLocomotion(String locomotion) {
        this.locomotion = locomotion;
    }

    @Override
    public void pond() {
        System.out.println(this.getNom() + " pond des oeufs.");
    }
}
