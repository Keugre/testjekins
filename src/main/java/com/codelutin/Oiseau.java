package com.codelutin;

public class Oiseau extends Animal implements Ovipare{

    protected boolean peutVoler;
    protected String formePattes;
    protected int nbOeufs;

    public Oiseau(){

    }

    @Override
    public void pond() {
        System.out.println(this.getNom() + " pond en moyenne " + this.nbOeufs + " œufs.");
    }
}
