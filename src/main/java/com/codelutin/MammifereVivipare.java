package com.codelutin;

public class MammifereVivipare extends Animal implements Vivipare {

    protected int moisAllaitement;
    protected String locomotion;

    public MammifereVivipare(){

    }

    public int getMoisAllaitement() {
        return moisAllaitement;
    }

    public void setMoisAllaitement(int moisAllaitement) {
        this.moisAllaitement = moisAllaitement;
    }

    public String getLocomotion() {
        return locomotion;
    }

    public void setLocomotion(String locomotion) {
        this.locomotion = locomotion;
    }

    @Override
    public void metBas() {
        System.out.println(this.getNom() + " met bas.");
    }
}
