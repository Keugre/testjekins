package com.codelutin;

public abstract class Animal {

    protected String nom;
    protected boolean systRespiratoire;
    protected String alimentation;
    protected boolean reproductionSexuee;
    protected int esperanceDeVie;
    protected String zone;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isSystRespiratoire() {
        return systRespiratoire;
    }

    public void setSystRespiratoire(boolean systRespiratoire) {
        this.systRespiratoire = systRespiratoire;
    }

    public String getAlimentation() {
        return alimentation;
    }

    public void setAlimentation(String alimentation) {
        this.alimentation = alimentation;
    }

    public boolean isReproductionSexuee() {
        return reproductionSexuee;
    }

    public void setReproductionSexuee(boolean reproductionSexuee) {
        this.reproductionSexuee = reproductionSexuee;
    }

    public int getEsperanceDeVie() {
        return esperanceDeVie;
    }

    public void setEsperanceDeVie(int esperanceDeVie) {
        this.esperanceDeVie = esperanceDeVie;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }
}
